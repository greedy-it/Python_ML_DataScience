import sys,re

for line in sys.stdin :
    help = {('null','0'),('eins','1'),('zwei','2'),('drei','3'),('vier','4'),('fuenf','5'),
            ('sechs','6'),('sieben','7'),('acht','8'),('neun','9'),('plus','+'),('minus','-'),
            ('mal','*'),('durch','/'),('gleich',""),('=',''),(' ','')}
    line = line.lower()
    for r in (help):
        line = line.replace(*r)
    line = re.sub("[^-?+?*?/?0-9^w]+", "", line)
    print(int(eval(line)))