"""
Hier eine Klasse `DataSet` implementieren.

Die Klasse muss eine Unterklasse von `dataset.DataSetInterface` sein
und alle dort geforderten Methoden implementieren.
Bei den Methoden von `DataSetInterface` stehe auch weitere Infos
zur benoetigen Funktionalitaet!

Die hier implementierte Klasse wird spaeter mittels `from implementation import DataSet`
geladen und mit Daten befüllt. Anschließend werden die Daten ausgelesen und überprüft!

Alle drei Dateien liegen im gleichen Ordner.

"""	

# TODO

# Importe
# Klasse: DataSet
import dataset

class DataSet(dataset.DataSetInterface):

    def __init__(self, items = []):
        super().__init__()
        self.foo = dict()

        for x in items:
            self.foo[x.name] = x


    def __setitem__(self, name, id_content):
        self.foo[name] = dataset.DataSetItem(name, id_content[0], id_content[1])

    def __iadd__(self, item):
        self.foo[item.name] = item
        return self

    def __delitem__(self, name):
        del self.foo[name]

    def __contains__(self, name):
        return name in self.foo

    def __getitem__(self, name):
        return self.foo[name]

    def __and__(self, dataset):
        dataset.newdata = dataset.foo
        dataset.foo = {x:self.foo[x] for x in self.foo if x in dataset.foo}
        return dataset

    def __or__(self, dataset):
        dataset.foo = dataset.newdata
        dataset.foo = {**self.foo, **dataset.foo}
        return dataset

    def __iter__(self):
        if self.iterate_sorted == False:
            for values in self.foo.values():
                yield values
        elif self.iterate_reversed == True:
            sorted_reverse_dict = sorted(self.foo.items(), key=lambda x: x[0], reverse=True)
            for values in dict(sorted_reverse_dict).values():
                yield values
        else:
            sorted_dict = sorted(self.foo.items(), key=lambda x: x[0])
            for values in dict(sorted_dict).values():
                yield values

    def filtered_iterate(self, filter):
        if self.iterate_sorted == False:
            for values in filter([name,id],True):
                yield values

    def __len__(self):
        return len(self.foo)




