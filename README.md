# Python for Machine Learning and Data Science

## Exercises

### 1. Textbasierter Taschenrechner in Python

### 2. Objektorientierung in Python

### 3. Git und LaTeX

### 4. Datenverarbeitung und Darstellung mit Python

### 5. Machine & Deep Learning mit Python
