import numpy as np
import matplotlib.pyplot as plt

def hellinger_distance(P, Q):
	"""
		Berechnet Hellinger-Distanz zwischen zwei Matrizen.

		Args:
			- P (np.ndarray): NumPy-Array der Matrix P
			- Q (np.ndarray): NumPy-Array der Matrix Q

		Returns:
			H (np.array) mit der Hellinger-Distanz zwischen P und Q
	"""
	# TODO

	# Nimm den Quadratwurzel jedes Elements von P und Q
	P_sqrt = np.sqrt(P)
	Q_sqrt = np.sqrt(Q)

	# Berechne die Differenz zwischen den Quadratwurzeln jedes Elements
	diff = P_sqrt - Q_sqrt

	# Berechne das Quadrat der Differenz
	diff_squared = diff ** 2

	# Berechne die Summe der quadrierten Differenz
	H = np.sum(diff_squared, axis=1)

	# Nimm den Quadratwurzel der Summe der quadrierten Differenz
	H = np.sqrt(H)

	# Multipliziere mit 1/sqrt(2)
	H = H * (1 / np.sqrt(2))

	return H

def select_best_rows(P, Q, H):
	"""
		Bestimmt die beiden Zeilen, die zwischen P und Q die minimale 
		Distanz zwischen P und Q repraesentieren (wobei die Distanz mittels
		H uebergeben wird).

		Args:
			- P (np.ndarray): NumPy-Array der Matrix P
			- Q (np.ndarray): NumPy-Array der Matrix Q
			- - H (np.array): Hellinger-Distanz-Vektors (wie z.B. von
				`hellinger_distance(P, Q)` ausgegeben)

		Returns:
			Matrix mit zwei Zeilen:

				[[Zeile mit Verteilung aus P]
				 [Zeile mit Verteilung aus Q]]
	"""
	# TODO

	min_index = np.argmin(H)
	return np.array([P[min_index], Q[min_index]])

	# return np.zeros(2, P.shape[1])
	
def plot_distance(H):
	"""
		Erstellt einen Bar-Plot des Hellinger-Distanz-Vektors.
		Grafik soll dem Beispiel auf dem Aufgabenblatt moeglichst nah
		sein! 

		Args:
			- H (np.array): Hellinger-Distanz-Vektors (wie z.B. von
				`hellinger_distance(P, Q)` ausgegeben)

		Returns:
			Das Plt-Modul von Matplotlib.

			Die Grafik soll dann z.B. durch Aufruf der Funktion `show()` auf den hier 
			zurueckgegebenen Wert angezeigt werden können.
	"""
	# TODO

	# Titel
	plt.title("Hellinger-Distanzen")

	# X-/Y-Achsen
	plt.xlabel("Verteilung (Matrixzeile)")
	plt.ylabel("Distanz")

	# Balkendiagramm
	plt.bar(np.arange(len(H)), H)

	# Legende
	plt.legend(["HD"])

	return plt
